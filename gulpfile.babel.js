import { src, dest, watch, series, parallel } from 'gulp';
import postcss from 'gulp-postcss';
import sourcemaps from 'gulp-sourcemaps';
import autoprefixer from 'autoprefixer';
import yargs from 'yargs';
import sass from 'sass'
import gulpSass from 'gulp-sass';
import cleanCss from 'gulp-clean-css';
import gulpif from 'gulp-if';
import imagemin from 'gulp-imagemin';
import del from 'del';
import fileinclude from 'gulp-file-include';
import include from 'gulp-include';
import webpack from 'webpack-stream';
import named from 'vinyl-named';
import browserSync from "browser-sync";
import MiniCssExtractPlugin from "mini-css-extract-plugin";

const SASS = gulpSass(sass);
const PRODUCTION = yargs.argv.prod;

// browserSync
export const sync = () => {
    browserSync.init({
        server: {
            baseDir: "./build"
        },
        open: false // true
    });
};

// Reload on watch().done
export const reload = (done) => {
    browserSync.reload();
    return done();
}

// Styles task
export const styles = () => {
    return src(['src/scss/app.scss'])
        .pipe(gulpif(!PRODUCTION, sourcemaps.init()))
        .pipe(SASS().on('error', SASS.logError))
        .pipe(gulpif(PRODUCTION, postcss([ autoprefixer ])))
        .pipe(gulpif(PRODUCTION, cleanCss({compatibility:'ie8'})))
        .pipe(gulpif(!PRODUCTION, sourcemaps.write()))
        .pipe(dest('./build/assets/css'))
        .pipe(browserSync.stream());
}

// Images task
export const images = () => {
    return src(['src/images/**/*.{jpg,jpeg,png,svg,gif,webmanifest}'])
        .pipe(gulpif(PRODUCTION, imagemin()))
        .pipe(dest('./build/assets/images'));
}

// Watch task
export const watchForChanges = () => {
    watch('src/scss/**/*.scss', styles);
    watch('src/images/**/*.{jpg,jpeg,png,svg,gif}', images);
    watch(['src/**/*','!src/{images,js,scss}','!src/{images,js,scss}/**/*'], copy);
    watch('src/js/**/*.js', scripts);
    watch(['src/**/*.html'], fileInclude);
}

// Copy task
export const copy = () => {
    return src(['src/**/*','!src/{images,js,scss,parts}','!src/{images,js,scss,parts}/**/*','!src/*.html'])
        .pipe(dest('./build'));
}

// Include libs styles files
export const includeCss = () => {
    return src(['node_modules/normalize.css/normalize.css'])
        .pipe(include())
        .on('error', console.log)
        .pipe(dest('./build/assets/css'));
}

// Include libs js files
export const includeJs = () => {
    return src(['node_modules/jquery/dist/jquery.min.js'])
        .pipe(include())
        .on('error', console.log)
        .pipe(dest('./build/assets/js'))
}

// Clean task
export const clean = () => del(['build']);

// Scripts task
export const scripts = () => {
    return src(['src/js/app.js'])
        .pipe(named())
        .pipe(webpack({
            module: {
                rules: [
                    {
                        test: /\.js$/,
                        use: {
                            loader: 'babel-loader',
                            options: {
                                presets: []
                            }
                        }
                    }
                ]
            },
            mode: PRODUCTION ? 'production' : 'development',
            devtool: !PRODUCTION ? 'inline-source-map' : false,
            output: {
                filename: '[name].js'
            }
        }))
        .pipe(dest('./build/assets/js'))
        .pipe(browserSync.stream());
}

// Production task
export const production = () => {
    return;
};

// Include html files task
export const fileInclude = () => {
    return src(['./src/*.html'])
        .pipe(fileinclude({
            prefix: '@@',
            basepath: './'
        }))
        .pipe(dest('./build'))
        .pipe(browserSync.stream());
};

// Convert svg icons to font task
export const iconFonts = () => {
    return;
    return src(['./src/js/iconsfont/entry.js'])
        .pipe(webpack({
            module: {
                rules: [
                    {
                        test: /\.font\.js/,
                        use: [
                            MiniCssExtractPlugin.loader,
                            {
                                loader: 'css-loader',
                                options: {
                                    url: false
                                }
                            },
                            {
                                loader: 'webfonts-loader',
                                options: {
                                    'publicPath': './',
                                    'files': [
                                        '../../images/icons/*.svg'
                                    ],
                                    'fontName': 'ea-icon',
                                    'classPrefix': 'eaicon-',
                                    'baseSelector': '.eaicon',
                                    'embed': false,
                                    'types': ['eot', 'woff', 'woff2', 'ttf', 'svg'],
                                    'fileName': '[fontname].[ext]'
                                }
                            }
                        ]
                    }
                ]
            },
            mode: PRODUCTION ? 'production' : 'development',
            plugins: [
                new MiniCssExtractPlugin({
                    filename: 'ea-icon.css'
                })
            ]
    })).pipe(dest('./build/fonts'));
}
// Gulp tasks bundles
export const includeLib = parallel(includeJs, includeCss);

export const svgToFonts = series(iconFonts);

export const _watch = series(clean, includeLib, parallel(fileInclude, styles, images, copy, scripts, sync, watchForChanges));

export const dev = series(clean, iconFonts, includeLib, parallel(fileInclude, styles, images, copy, scripts));

export const build = series(clean, iconFonts, includeLib, parallel(fileInclude, styles, images, copy, scripts));

// Define gulp default task
export default dev;


